﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gwen
{
	public class Cursor
	{
		public static Cursor Current;
		public int id;

		public Cursor(int id)
		{
			this.id = id;
		}
	}

	public class Cursors
	{
		public static Cursor Default = new Cursor(0);
		public static Cursor No = new Cursor(1);
		public static Cursor SizeAll = new Cursor(2);
		public static Cursor SizeNESW = new Cursor(3);
		public static Cursor SizeNS = new Cursor(4);
		public static Cursor SizeNWSE = new Cursor(5);
		public static Cursor SizeWE = new Cursor(6);
	}
}