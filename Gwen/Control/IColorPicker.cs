﻿using System;
using Microsoft.Xna.Framework;

namespace Gwen.Control
{
	public interface IColorPicker
	{
		Color SelectedColor { get; }
	}
}