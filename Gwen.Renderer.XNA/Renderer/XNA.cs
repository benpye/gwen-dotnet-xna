﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Color = Microsoft.Xna.Framework.Color;

namespace Gwen.Renderer
{
	/// <summary>
	/// XNA renderer.
	/// </summary>
	public class XNA : Renderer.Base
	{
		private sealed class XNATexture
		{
			public string Name { get; set; }
			public Texture2D Tex { get; set; }
			public Color[,] Data { get; set; }
			public Texture GTex { get; set; }
		}

		private sealed class XNAFont
		{
			public Font Resource { get; set; }
			public SpriteFont Fnt { get; set; }
		}

		private RenderTarget2D m_Target;
		private Color m_Color;

		private SpriteBatch m_Batch;
		private GraphicsDevice m_Device;

		private Texture2D m_WhiteTex;

		public bool UseRT { get; set; }

		private List<XNATexture> m_TexToLoad;
		private List<XNAFont> m_FntToLoad;

		private Texture2D m_MissingImage;

		public ContentManager Content { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="XNA"/> class.
		/// </summary>
		/// <param name="target">XNA render target.</param>
		public XNA(RenderTarget2D target, GraphicsDevice device)
		{
			m_Target = target;
			m_Device = device;
			m_Batch = new SpriteBatch(device);
			m_TexToLoad = new List<XNATexture>();
			m_FntToLoad = new List<XNAFont>();
			//UseRT = true;
		}

		public void ChangeTarget(RenderTarget2D target, GraphicsDevice device)
		{
			m_Target = target;
			m_Device = device;
		}

		public void LoadTextures(ContentManager content)
		{
			foreach (XNATexture tex in m_TexToLoad) _LoadTexture(tex, content);
			Color[] flat = new Color[4] {Color.White, Color.White, Color.White, Color.White};
			m_WhiteTex = new Texture2D(m_Device, 2, 2);
			m_WhiteTex.SetData<Color>(flat);

			m_MissingImage = content.Load<Texture2D>("missing");

			m_TexToLoad.Clear();
		}

		private void _LoadTexture(XNATexture tex, ContentManager content)
		{
			tex.Name = tex.Name.Replace(".png", "");
			Texture2D raw = content.Load<Texture2D>(tex.Name);
			tex.Tex = raw;
			Color[] flat = new Color[raw.Width*raw.Height];
			raw.GetData<Color>(flat);
			tex.Data = new Color[raw.Width,raw.Height];
			for (int x = 0; x < raw.Width; x++)
				for (int y = 0; y < raw.Height; y++)
					tex.Data[x, y] = flat[x + (y*raw.Width)];

			tex.GTex.Width = raw.Width;
			tex.GTex.Height = raw.Height;
		}

		public void LoadFonts(ContentManager content)
		{
			foreach (XNAFont font in m_FntToLoad) _LoadFont(font, content);
			m_FntToLoad.Clear();
		}

		private bool _LoadFont(XNAFont font, ContentManager content)
		{
			try
			{
				SpriteFont raw = content.Load<SpriteFont>(font.Resource.FaceName);
				font.Fnt = raw;
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public override void Begin()
		{
			if (UseRT)
			{
				m_Device.SetRenderTarget(m_Target);
				m_Device.Clear(Color.Black);
			}

			m_Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
		}

		public override void End()
		{
			m_Batch.End();
			if (UseRT) m_Device.SetRenderTarget(null);
		}

		/// <summary>
		/// Gets or sets the current drawing color.
		/// </summary>
		public override Microsoft.Xna.Framework.Color DrawColor
		{
			get { return new Color(m_Color.R, m_Color.G, m_Color.B, m_Color.A); }
			set { m_Color = new Color(value.R, value.G, value.B, value.A); }
		}

		public override Microsoft.Xna.Framework.Color PixelColor(Texture texture, uint x, uint y,
		                                                         Microsoft.Xna.Framework.Color defaultColor)
		{
			XNATexture tex = texture.RendererData as XNATexture;
			if (tex == null)
				return defaultColor;
			if (tex.Data == null)
				return defaultColor;
			Color pixel = tex.Data[x, y];
			return new Color(pixel.R, pixel.G, pixel.B, pixel.A);
		}

		public override void DrawLine(int x, int y, int a, int b)
		{
			Translate(ref x, ref y);
			Translate(ref a, ref b);
			VertexPositionColor[] line = new VertexPositionColor[2]
			                             	{
			                             		new VertexPositionColor(new Vector3(x, y, 0f), m_Color),
			                             		new VertexPositionColor(new Vector3(x + a, y + b, 0f), m_Color)
			                             	};
			short[] indices = new short[2] {0, 1};
			m_Device.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineList, line, 0, 2, indices, 0, 1);
		}

		public override void DrawFilledRect(Rectangle rect)
		{
			rect = Translate(rect);
			m_Batch.Draw(m_WhiteTex, new Microsoft.Xna.Framework.Rectangle(rect.X, rect.Y, rect.Width, rect.Height), m_Color);
		}

		/// <summary>
		/// Loads the specified font.
		/// </summary>
		/// <param name="font">Font to load.</param>
		/// <returns>True if succeeded.</returns>
		public override bool LoadFont(Font font)
		{
			XNAFont raw = new XNAFont();
			font.RendererData = raw;
			font.RealSize = font.Size*Scale;
			raw.Fnt = null;
			raw.Resource = font;

			if (Content != null)
				return _LoadFont(raw, Content);
			else
				m_FntToLoad.Add(raw);

			return true;
		}

		/// <summary>
		/// Frees the specified font.
		/// </summary>
		/// <param name="font">Font to free.</param>
		public override void FreeFont(Font font)
		{
			if (font.RendererData == null) return;
			XNAFont raw = font.RendererData as XNAFont;
			raw.Fnt = null;
			raw.Resource = null;
			font.RendererData = null;
			// Let the GC clean it up for us
		}

		public override void RenderText(Font font, Point pos, string text)
		{
			pos = Translate(pos);
			XNAFont raw = font.RendererData as XNAFont;

			// If the font doesn't exist, or the font size should be changed
			if (raw == null || Math.Abs(font.RealSize - font.Size*Scale) > 2)
			{
				FreeFont(font);
				LoadFont(font);
				return;
			}

			if (raw.Fnt == null)
				return;

			m_Batch.DrawString(raw.Fnt, text, new Vector2(pos.X, pos.Y), m_Color);
		}

		/// <summary>
		/// Returns dimensions of the text using specified font.
		/// </summary>
		/// <param name="font">Font to use.</param>
		/// <param name="text">Text to measure.</param>
		/// <returns>
		/// Width and height of the rendered text.
		/// </returns>
		public override Point MeasureText(Font font, string text)
		{
			XNAFont raw = font.RendererData as XNAFont;

			if (raw == null)
			{
				FreeFont(font);
				if (LoadFont(font) == false)
					return new Point(50, 20);
			}

			raw = font.RendererData as XNAFont;

			if (raw.Fnt == null)
				return new Point(50, 20);

			Vector2 sz = raw.Fnt.MeasureString(text);
			return new Point((int) sz.X, (int) sz.Y);
		}

		public override void DrawTexturedRect(Texture t, Rectangle targetRect, float u1 = 0, float v1 = 0, float u2 = 1,
		                                      float v2 = 1)
		{
			XNATexture tex = t.RendererData as XNATexture;
			if (null == tex)
			{
				DrawMissingImage(targetRect);
				return;
			}

			DrawTexturedRect(tex.Tex, targetRect, u1, v1, u2, v2);
		}

		protected void DrawTexturedRect(Texture2D tex, Rectangle targetRect, float u1 = 0, float v1 = 0, float u2 = 1,
		                                float v2 = 1)
		{
			if (tex == null)
				tex = m_MissingImage;

			Rectangle rect = Translate(targetRect);

			int x1 = (int) (u1*tex.Width);
			int y1 = (int) (v1*tex.Height);
			int w = (int) ((u2 - u1)*tex.Width);
			int h = (int) ((v2 - v1)*tex.Height);

			m_Batch.Draw(tex,
			             new Microsoft.Xna.Framework.Rectangle(rect.X, rect.Y, rect.Width, rect.Height),
			             new Microsoft.Xna.Framework.Rectangle?(new Microsoft.Xna.Framework.Rectangle(x1, y1, w, h)),
			             m_Color);
		}

		public override void LoadTexture(Texture texture)
		{
			if (null == texture) return;

			//Debug.Print("LoadTexture: {0} {1}", texture.Name, texture.RendererData);

			if (texture.RendererData != null)
				FreeTexture(texture);

			XNATexture raw = new XNATexture();
			raw.Name = texture.Name;
			raw.Tex = null;
			raw.Data = null;
			raw.GTex = texture;
			texture.RendererData = raw;

			try
			{
				if (Content != null)
					_LoadTexture(raw, Content);
				else
					m_TexToLoad.Add(raw);
			}
			catch (Exception)
			{
				texture.Failed = true;
			}
		}

		public override void LoadTextureRaw(Texture texture, byte[] pixelData)
		{
			if (null == texture) return;

			//Debug.Print("LoadTextureRaw: {0}", texture.RendererData);

			if (texture.RendererData != null)
				FreeTexture(texture);

			XNATexture raw = new XNATexture();
			raw.Name = texture.Name;
			raw.GTex = texture;

			Texture2D tex = new Texture2D(m_Device, texture.Width, texture.Height);
			tex.SetData<byte>(pixelData);
			raw.Tex = tex;

			Color[] flat = new Color[texture.Width*texture.Height];
			tex.GetData<Color>(flat);
			raw.Data = new Color[texture.Width,texture.Height];
			for (int x = 0; x < texture.Width; x++)
				for (int y = 0; y < texture.Height; y++)
					raw.Data[x, y] = flat[x + (y*texture.Width)];
		}

		public override void FreeTexture(Texture texture)
		{
			XNATexture raw = texture.RendererData as XNATexture;
			if (raw != null)
			{
				//raw.Tex.Dispose();
				raw.Tex = null;
				raw.GTex = null;
				raw.Data = null;
			}

			//Debug.Print("FreeTexture: {0} {1}", texture.Name, texture.RendererData);

			texture.RendererData = null;
		}

		public override void StartClip()
		{
			m_Batch.End();
			Rectangle rect = ClipRegion;
			m_Device.ScissorRectangle = new Microsoft.Xna.Framework.Rectangle((int) (rect.X*Scale), (int) (rect.Y*Scale),
			                                                                  (int) (rect.Width*Scale), (int) (rect.Height*Scale));
			m_Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, new RasterizerState()
			                                                                          	{
			                                                                          		ScissorTestEnable = true,
			                                                                          	});
		}

		public override void EndClip()
		{
			m_Batch.End();
			m_Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
		}
	}
}