﻿using System;
using System.Collections.Generic;
using Gwen.Control;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Gwen.Input
{
	public class XNA
	{
		#region Properties

		private Canvas m_Canvas = null;

		private int m_MouseX = 0;
		private int m_MouseY = 0;

		private MouseState m_PrevMouseState;
		private KeyboardState m_PrevKBState;

		#endregion

		#region Constructors

		public XNA()
		{
		}

		#endregion

		#region Methods

		public void Initialize(Canvas c)
		{
			m_Canvas = c;
		}


		/// <summary>
		/// Translates control key's XNA key code to GWEN's code.
		/// </summary>
		/// <param name="sfKey">XNA key code.</param>
		/// <returns>GWEN key code.</returns>
		private static Key TranslateKeyCode(global::Microsoft.Xna.Framework.Input.Keys xnaKey)
		{
			switch (xnaKey)
			{
				case global::Microsoft.Xna.Framework.Input.Keys.Back:
					return Key.Backspace;
				case global::Microsoft.Xna.Framework.Input.Keys.Enter:
					return Key.Return;
				case global::Microsoft.Xna.Framework.Input.Keys.Escape:
					return Key.Escape;
				case global::Microsoft.Xna.Framework.Input.Keys.Tab:
					return Key.Tab;
				case global::Microsoft.Xna.Framework.Input.Keys.Space:
					return Key.Space;
				case global::Microsoft.Xna.Framework.Input.Keys.Up:
					return Key.Up;
				case global::Microsoft.Xna.Framework.Input.Keys.Down:
					return Key.Down;
				case global::Microsoft.Xna.Framework.Input.Keys.Left:
					return Key.Left;
				case global::Microsoft.Xna.Framework.Input.Keys.Right:
					return Key.Right;
				case global::Microsoft.Xna.Framework.Input.Keys.Home:
					return Key.Home;
				case global::Microsoft.Xna.Framework.Input.Keys.End:
					return Key.End;
				case global::Microsoft.Xna.Framework.Input.Keys.Delete:
					return Key.Delete;
				case global::Microsoft.Xna.Framework.Input.Keys.LeftControl:
					return Key.Control;
				case global::Microsoft.Xna.Framework.Input.Keys.LeftAlt:
					return Key.Alt;
				case global::Microsoft.Xna.Framework.Input.Keys.LeftShift:
					return Key.Shift;
				case global::Microsoft.Xna.Framework.Input.Keys.RightControl:
					return Key.Control;
				case global::Microsoft.Xna.Framework.Input.Keys.RightAlt:
					return Key.Alt;
				case global::Microsoft.Xna.Framework.Input.Keys.RightShift:
					return Key.Shift;
			}
			return Key.Invalid;
		}


		/// <summary>
		/// Translates alphanumeric XNA key code to character value.
		/// </summary>
		/// <param name="sfKey">XNA key code.</param>
		/// <returns>Translated character.</returns>
		private static char TranslateChar(global::Microsoft.Xna.Framework.Input.Keys xnaKey)
		{
			if (xnaKey >= global::Microsoft.Xna.Framework.Input.Keys.A && xnaKey <= global::Microsoft.Xna.Framework.Input.Keys.Z)
				if (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || Keyboard.GetState().IsKeyDown(Keys.RightShift))
				{
					return (char)('A' + ((int)xnaKey - (int)global::Microsoft.Xna.Framework.Input.Keys.A));
				}
				else
				{
					return (char)('a' + ((int)xnaKey - (int)global::Microsoft.Xna.Framework.Input.Keys.A));
				}

			if (xnaKey >= global::Microsoft.Xna.Framework.Input.Keys.NumPad0 &&
				xnaKey <= global::Microsoft.Xna.Framework.Input.Keys.NumPad9)
				return (char)('0' + ((int)xnaKey - (int)global::Microsoft.Xna.Framework.Input.Keys.NumPad0));

			if (xnaKey >= global::Microsoft.Xna.Framework.Input.Keys.D0 &&
				xnaKey <= global::Microsoft.Xna.Framework.Input.Keys.D9)
				return (char)('0' + ((int)xnaKey - (int)global::Microsoft.Xna.Framework.Input.Keys.D0));

			if (xnaKey == Keys.OemPeriod)
				return '.';

			if (xnaKey == Keys.Space)
				return ' ';

			throw new Exception();
		}

		public bool ProcessMouseMove(MouseState state)
		{
			return ProcessMouseMove(state.X, state.Y);
		}

		public bool ProcessMouseMove(int x, int y)
		{
			if (null == m_Canvas) return false;

			int dx = x - m_MouseX;
			int dy = y - m_MouseY;

			if ((dx == 0) && (dy == 0)) return false;

			m_MouseX = x;
			m_MouseY = y;

			return m_Canvas.Input_MouseMoved(m_MouseX, m_MouseY, dx, dy);
		}

		public bool ProcessMouseClick(MouseState state)
		{
			if (null == m_Canvas) return false;

			bool result = false;

			if (state.LeftButton != m_PrevMouseState.LeftButton)
				result = result || m_Canvas.Input_MouseButton(0, state.LeftButton == ButtonState.Pressed);

			if (state.RightButton != m_PrevMouseState.RightButton)
				result = result || m_Canvas.Input_MouseButton(1, state.RightButton == ButtonState.Pressed);

			m_PrevMouseState = state;

			return result;
		}

		public bool ProcessKeyPress(KeyboardState state)
		{
			if (null == m_Canvas) return false;

			bool result = false;

			Keys[] keys = state.GetPressedKeys();
			Keys[] prevkeys = m_PrevKBState.GetPressedKeys();

			// See if the key is NOT present in the other array

			foreach (Keys key in keys)
			{
				bool found = false;
				foreach (Keys pkey in prevkeys)
				{
					found = true;
					break;
				}
				if (!found)
					// Keydown!
					result = result || KeyDown(key);
			}

			// See if the key is present in the other array but NOT present in our array
			foreach (Keys pkey in prevkeys)
			{
				if (state.IsKeyUp(pkey))
				{
					try
					{
						char ch = TranslateChar(pkey);
						InputHandler.HandleAccelerator(m_Canvas, ch);

						if (!InputHandler.DoSpecialKeys(m_Canvas, ch))
							m_Canvas.Input_Character(ch);
					}
					catch (Exception)
					{

					}

				}

				bool found = false;
				foreach (Keys key in keys)
				{
					found = true;
					break;
				}
				if (!found)
					// Keyup!
					result = result || KeyUp(pkey);
			}

			m_PrevKBState = state;

			return result;
		}

		private bool KeyDown(Keys key)
		{
			//char ch = TranslateChar(key);
			Key iKey = TranslateKeyCode(key);

			return m_Canvas.Input_Key(iKey, true);
		}

		private bool KeyUp(Keys key)
		{
			Key iKey = TranslateKeyCode(key);
			return m_Canvas.Input_Key(iKey, false);
		}

		#endregion
	}
}