using System;

namespace Gwen.Sample.XNA
{
#if WINDOWS || XBOX
	internal static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		private static void Main(string[] args)
		{
			using (Sample game = new Sample())
			{
				game.Run();
			}
		}
	}
#endif
}