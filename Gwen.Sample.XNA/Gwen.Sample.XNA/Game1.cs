using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#if WINDOWS_PHONE
using Microsoft.Xna.Framework.Input.Touch;
#endif
using Microsoft.Xna.Framework.Media;
using Gwen.Control;
using Gwen;
using Color = Microsoft.Xna.Framework.Color;

namespace Gwen.Sample.XNA
{
	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class Sample : Microsoft.Xna.Framework.Game
	{
		private GraphicsDeviceManager graphics;

		private SpriteBatch batch;

		private Input.XNA gwenInput;
		private Renderer.XNA gwenRenderer;
		private Skin.Base m_Skin;

		private Canvas canvas;
		private UnitTest.UnitTest m_UnitTest;

		private RenderTarget2D m_RT2D; // Not quite R2D2

		private Texture2D[] cursors;
		private Vector2 mousePos;

		public Sample()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";

#if WINDOWS
			graphics.PreferredBackBufferWidth = 1024;
			graphics.PreferredBackBufferHeight = 768;
#endif
#if XBOX360
			graphics.PreferredBackBufferWidth = 1280;
			graphics.PreferredBackBufferHeight = 720;
#endif
#if WINDOWS_PHONE
			graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft;
			graphics.IsFullScreen = true;
#endif
			graphics.PreferMultiSampling = true;
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			m_RT2D = new RenderTarget2D(this.GraphicsDevice, graphics.PreferredBackBufferWidth,
			                            graphics.PreferredBackBufferHeight);

			gwenRenderer = new Renderer.XNA(m_RT2D, this.GraphicsDevice);
			gwenRenderer.Content = Content;

			m_Skin = new Skin.TexturedBase(gwenRenderer, "DefaultSkin");
			m_Skin.SetDefaultFont("Arial");

			canvas = new Canvas(m_Skin);
			canvas.SetSize(m_RT2D.Width, m_RT2D.Height);
			canvas.ShouldDrawBackground = false;
			canvas.BackgroundColor = Microsoft.Xna.Framework.Color.CornflowerBlue;
			canvas.KeyboardInputEnabled = true;

			gwenRenderer.LoadTextures(this.Content);
			gwenRenderer.LoadFonts(this.Content);

			m_UnitTest = new UnitTest.UnitTest(canvas);

			gwenInput = new Input.XNA();
			gwenInput.Initialize(canvas);

			batch = new SpriteBatch(this.GraphicsDevice);

			cursors = new Texture2D[7];
			cursors[0] = Content.Load<Texture2D>("cursors/cursor-arrow");
			cursors[1] = Content.Load<Texture2D>("cursors/cursor-forbidden");
			cursors[2] = Content.Load<Texture2D>("cursors/cursor-sizeall");
			cursors[3] = Content.Load<Texture2D>("cursors/cursor-sizeb");
			cursors[4] = Content.Load<Texture2D>("cursors/cursor-sizev");
			cursors[5] = Content.Load<Texture2D>("cursors/cursor-sizef");
			cursors[6] = Content.Load<Texture2D>("cursors/cursor-sizeh");
			mousePos = new Vector2(50, 50);
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// all content.
		/// </summary>
		protected override void UnloadContent()
		{
			gwenRenderer.Dispose();
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			if (Keyboard.GetState().IsKeyDown(Keys.Escape))
			{
				this.Exit();
				return;
			}

			KeyboardState kbState = Keyboard.GetState();
#if WINDOWS
			MouseState mState = Mouse.GetState();
			mousePos.X = mState.X;
			mousePos.Y = mState.Y;
#endif

#if XBOX360
			GamePadState gState = GamePad.GetState(0);
			mousePos.X += gState.ThumbSticks.Left.X * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
			mousePos.Y -= gState.ThumbSticks.Left.Y * (float)gameTime.ElapsedGameTime.TotalMilliseconds;

			MouseState mState = new MouseState((int)mousePos.X, (int)mousePos.Y, 0, gState.Buttons.A, ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released);
#endif

#if WINDOWS_PHONE
			ButtonState touch = ButtonState.Released;
			TouchPanelCapabilities tc = TouchPanel.GetCapabilities();
			if (tc.IsConnected)
			{
				TouchCollection tState = TouchPanel.GetState();

				foreach (TouchLocation touchLocation in tState)
				{
					if (touchLocation.State == TouchLocationState.Moved || touchLocation.State == TouchLocationState.Pressed)
					{
						touch = ButtonState.Pressed;
						mousePos = touchLocation.Position;
					}
				}
			}

			MouseState mState = new MouseState((int)mousePos.X, (int)mousePos.Y, 0, touch, ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released);
#endif

			gwenInput.ProcessKeyPress(kbState);
			gwenInput.ProcessMouseMove(mState);
			gwenInput.ProcessMouseClick(mState);

			m_UnitTest.Fps = 1d/gameTime.ElapsedGameTime.TotalSeconds;

			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.SetRenderTarget(m_RT2D);
			GraphicsDevice.Clear(Color.Transparent);
			canvas.RenderCanvas();

			GraphicsDevice.SetRenderTarget(null);
			GraphicsDevice.Clear(Color.CornflowerBlue);
			// Basic fullscreen 2D
			batch.Begin();
			batch.Draw(m_RT2D, Vector2.Zero, Color.White);
			batch.End();

#if WINDOWS || XBOX360
			batch.Begin();
			batch.Draw(cursors[gwenRenderer.GetCursor().id], mousePos, Color.White);
			batch.End();
#endif

			base.Draw(gameTime);
		}
	}
}